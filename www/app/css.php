<?php
class Css extends Controller {

	function channels($f3) {
		$db = $f3->get('DB');
		$statement = $db->prepare('SELECT * FROM channels WHERE active = 1');
		$statement->execute();
		header("Content-Type: text/less");
		while($chan = $statement->fetch()) {
			echo ".chan-{$chan['slug']} {\n";
			echo "\tcolor: lighten( spin({$f3->get('color.modcolor')},{$chan['hue']}), 30%);\n";
			echo "}\n";
			echo "button.btn.chan-{$chan['slug']} {\n";
			echo "\tbackground-color: darken( spin({$f3->get('color.modcolor')},{$chan['hue']}), 10%);\n";
			echo "\tborder:spin({$f3->get('color.modcolor')},{$chan['hue']});\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} {\n";
			echo "\tbackground-color: spin({$f3->get('color.modcolor')},{$chan['hue']});\n";
			echo "\tpadding:5px;\n";
			echo "\tborder-radius:5px;\n";
			echo "\tmargin-bottom:10px;\n";
			echo "\ttext-align:center;\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} p {\n";
			echo "\tpadding:5px;\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} legend {\n";
			echo "\tpadding:5px;\n";
			echo "\tborder-radius:5px;\n";
			echo "\tbackground-color: darken( spin({$f3->get('color.modcolor')},{$chan['hue']}),10%);\n";
			echo "\tcolor:white;\n";
			echo "\ttext-align:left;\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} legend i {\n";
			echo "\tcolor: lighten( spin({$f3->get('color.modcolor')},{$chan['hue']}), 40%);\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} button {\n";
			echo "\tbackground-color: darken( spin({$f3->get('color.modcolor')},{$chan['hue']}),10%);\n";
			echo "\ttext-align:center;\n";
			echo "\tmargin: 0 auto;\n";
			echo "\tcolor: lighten( spin({$f3->get('color.modcolor')},{$chan['hue']}), 40%);\n";
			echo "}\n";
			echo "fieldset.chan-{$chan['slug']} button:hover {\n";
			echo "\tcolor: lighten( spin({$f3->get('color.modcolor')},{$chan['hue']}), 50%);\n";
			echo "\ttext-decoration:none;\n";
			echo "}\n";
		}
	}

}
?>
