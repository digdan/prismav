<?php
class Cron extends Controller {
	function beforeRoute($f3,$params) {
		if (($f3->get('STAGE') == "PROD") and (php_sapi_name() != "cli")) die(''); //All things cron execute through the CLI
	}

	function eatSampleQue($f3,$params=NULL) {
		$que = \Que::instance();
		$que->all('samples'); //Load and flush out any incorrect records
		$jobCount = 0;
		$job_id = NULL;
		while ( $job_id = $que->reserve('samples') ) {
			$job = new \Prismav\SampleJob( $que->data($job_id) );
			$notify = new Notifications( $job->data['author'] );
			if ($job->perform($f3) === TRUE) { //Job performed successfullly
				$notify->addNotification('Sample Proccess Complete','Sample Job '.$job_id.' in channel <A href="/admin/'.$job->data['channel'].'/samples">'.$job->data('channel').'</A> has completed');
				$que->complete($job_id,true);
			} else { //Job failed
				$error = $job->getError();
				$notify->addNotification('Sample Process Failed','There was a problem with '.$job_id.' in channel '.$job->data('channel').' Error : '.$error,'fa fa-warning');
				$que->complete($job_id,false);
			}
			$jobCount++;
			$job->cleanup();
			return;
		}
		echo "{$jobCount} sample jobs performed\n";
	}

	function eatSongQue($f3,$params=NULL) {
		$que = \Que::instance();
		$jobCount=0;
		//Eat Song Que
		$job_id = NULL;
		while ( $job_id = $que->reserve('songs') ) {
			$job = new \Prismav\SongJob( $que->data($job_id) );
			$que->complete($job_id, $job->perform() );
			$jobCount++;
			$job->cleanup();
		}
		echo "{$jobCount} song jobs performed\n";
	}


	function showQues($f3,$params) {
		$que = \Que::instance();
		$count = 0;
		$filters = $f3->get('FILTER');
		echo "# Samples \n";
		foreach($que->all('samples') as $key=>$item) {
			$count++;
			echo "\t{$count}. {$key}\n";
			foreach ($item as $k=>$v) {
				if ($k == 'created') {
					$v = date("r",$v);
				}
				if ($k == 'author') {
					$v = $filters['userId']($v);
				}

				echo "\t\t{$k} : {$v}\n";
			}
		}

		$count = 0;
		echo "# Songs \n";
		foreach($que->all('songs') as $key=>$item) {
			echo "\t{$count}. {$key}\n";
			foreach ($item as $k=>$v) {
				echo "\t\t{$k} : {$v}\n";
			}
		}

		echo "# End of List\n";
	}
}
?>
