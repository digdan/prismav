<?php
	class Controller {
		function beforeRoute($f3,$params)  {
			//Global controller pre route hook to handle channel definitions
			if ($channelSlug = $f3->get('PARAMS.channel')) {
				$db = $f3->get('DB');
				$statement = $db->prepare("SELECT * FROM channels WHERE slug = ? and active=1");
				$statement->execute(array($channelSlug));
				if ($chan = $statement->fetch()) {
					$f3->set('chan',$chan);
					$f3->set('channel',$chan['name']);
					$f3->set('channelSlug',$chan['slug']);
					$f3->set('hue',$chan['hue']);
					$f3->set('desaturation','0%');
					if (file_exists("ui/sidebars/{$chan['slug']}.htm")) $f3->set('sidebar','sidebars/'.$chan['slug'].".htm");
				}
			}
		}
	}
?>
