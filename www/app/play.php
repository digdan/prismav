<?php
class Play extends Controller {
	function main($f3,$params) {
		$f3->set('content','play.htm');
		echo \Template::instance()->render('layout.htm');
	}

	function channels($f3) {
		$db = $f3->get('DB');
		$statement = $db->prepare("SELECT * from channels WHERE active = 1");
		$statement->execute();
		$channels = array();
		while($chan = $statement->fetch()) {
			$channels[] = $chan;
		}
		$f3->set('channels',$channels);
		$f3->set('content','channels.htm');
		echo \Template::instance()->render('layout.htm');
	}

	function uploadProgress($f3,$params) {
		$key = ini_get("session.upload_progress.prefix").$params['item'];
		if (!empty($_SESSION[$key])) {
			$current = $_SESSION[$key]["bytes_processed"];
			$total = $_SESSION[$key]["content_length"];
			echo $current < $total ? ceil($current / $total * 100) : 100;
		} else {
			echo 100;
		}
		die();
	}
}
?>
