<?php
class User extends Controller {
	function logout($f3) {
		session_start();
		session_destroy();
		header('HTTP/1.0 401 Unauthorized');
		//header("Location: /");
                $f3->set('content','logout.htm');
                echo \Template::instance()->render('layout.htm');
	}

	static function checkAuth($f3) {
		if ($_SERVER['REQUEST_URI'] != "/logout") {
			$admin = new \DB\SQL\Mapper($f3->get('DB'), 'admins');
			if ($_SERVER['PHP_AUTH_USER']) {
				$user = $admin->load(array('email=?',$_SERVER['PHP_AUTH_USER']));
				$f3->set('USER',$user);
				$notify = new Notifications($user->get('id'));
				$f3->set('NOTIFY',$notify);
				$f3->set('NOTIFICATIONS',$notify->countUnread());
                		$f3->set('ques', array(
                		        'samples'=>\Que::instance()->size('samples'),
                		        'songs'=>\Que::instance()->size('songs'),
                		));
				return true;
			}
			return false;
		}
		return false;
	}

	static function userID($f3) {
		if ($_SERVER['PHP_AUTH_USER']) {
			$query = "SELECT id FROM admins WHERE email=?";
			$q = $f3->get('DB')->prepare($query);
			$q->execute(array($_SERVER['PHP_AUTH_USER']));
			if ($row = $q->fetch()) {
				return $row['id'];
			} return false;
		} else return false;
	}

	static function userInfo($f3,$id) {
		$admin = new \DB\SQL\Mapper($f3->get('DB'), 'admins');
		return $admin->load(array('id=?',$id));
	}
}
?>
