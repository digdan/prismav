<?php
class Admin extends Controller {
	function beforeRoute($f3,$params) {
		parent::beforeRoute($f3,$params);
		$admin = new \DB\SQL\Mapper($f3->get('DB'), 'admins');
		$auth = new \Auth($admin, array('id'=>'email','pw'=>'password'));
		if (!$auth->basic('md5')) {
			die('Access denied!');
		}
		$f3->set('USER',$admin->load(array('email=?',$_SERVER['PHP_AUTH_USER'])));
		$f3->set('ADMIN',true);
		$que = \Que::instance()->all('samples'); //Clear out any old samples
	}

	function home($f3) {
                $db = $f3->get('DB');
                $statement = $db->prepare("SELECT * from channels WHERE active = 1");
                $statement->execute();
                $channels = array();
                while($chan = $statement->fetch()) {
                        $channels[] = $chan;
                }
                $f3->set('channels',$channels);
		$f3->set('content','admin/home.htm');
		echo \Template::instance()->render('layout.htm');
	}

	function notifications($f3) {
		$notify = $f3->get('NOTIFY');
		$f3->set('notifyList',$notify->getList());
		$f3->set('lastView',$notify->get('lastView'));
		$f3->set('content','admin/notifications.htm');
		$notify->markRead();
		echo \Template::instance()->render('layout.htm');
	}

	function samplesQue($f3) {
		$f3->set('content','admin/samplesQue.htm');		
		if ($_REQUEST['rm']) { //Looking to remove sample from que
			if ($jd = \Que::instance()->data($_REQUEST['rm'])) {
				$ad = User::userInfo($f3,$jd['author']);
				if ($ad->email == $_SERVER['PHP_AUTH_USER']) {
					$s3 = new S3($f3->get('s3.access_key'),$f3->get('s3.secret_key'),$f3->get('s3.endpointTemp'));
					$urlp = parse_url($jd["url"]);
					$response = $s3->deleteObject( $f3->get('s3.bucket_temp'), basename($urlp["path"]) );
					\Que::instance()->complete($_REQUEST['rm'],true); //Remove from que
					User::checkAuth($f3);
				}
			}
		}
		$que = \Que::instance()->all('samples');
		foreach($que as $k=>$v) {
			$que[$k]['status'] = \Que::instance()->status($v['id']);
			if ($v['author']) $que[$k]['authorData'] = User::userInfo($f3,$v['author']);
		}
		$f3->set('que',array_reverse( $que ));
		echo \Template::instance()->render('layout.htm');
	}
	function songsQue($f3) {
		$f3->set('content','admin/songsQue.htm');
		$f3->set('que',\Que::instance()->all('songs'));
		echo \Template::instance()->render('layout.htm');
	}

	function songs($f3,$params) {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') { //Update/Create a song			
			if (isset($_POST['song_id']) and ($_POST['song_id'] <> '')) { //Update
				$song = new \Prismav\Song($_REQUEST['song_id']);				
				if ($song->get('user_id') != User::userID($f3)) return false;
				$recipe = $song->getRecipe();
			} else { //Create
				$song = new \Prismav\Song();
				$song->setAuthor( User::userID( $f3 ));
				$recipe = $song->getRecipe();
			}
			$song->setInfo(array(
				'title'=>$_POST['name'],
				'description'=>$_POST['description'],
			));
			if ($_POST['base_id'] <> $recipe->samples[0]['sample_id']) { //If the base sample id is updated, change the recipe.
				$recipe->samples[0] = array(
					'sample_id'=>$_POST['base_id']
				);
				$song->setRecipe($recipe);
			}
			$song->save();	
		} elseif ($_REQUEST['load']) {
			$song = new \Prismav\Song($_REQUEST['load']);
			echo json_encode($song->fieldDump());
			die();
		}
		$f3->set('songs', \Prismav\Song::fromChannel($params['channel']));
		$f3->set('bases', \Prismav\Sample::fromChannel($params['channel'],User::userID($f3),true));
		$f3->set('content','admin/songs.htm');
		$f3->set('FULL',true);
		echo \Template::instance()->render('layout.htm');
	}

	function songEdit($f3,$params) {
		$song = new\Prismav\Song($params['id']);
		if ($song->get('user_id') != User::userID($f3)) {
			die('Not your song buddy.');
		}
		var_dump($params);
	}

	function samples($f3,$params) {
		if ($_REQUEST['rm']) {
			//Remove sample			
			$sample = new \Prismav\Sample($_REQUEST['rm']);
			if ($sample->get('user_id') == User::userID($f3)) {
				$s3 = new S3($f3->get('s3.access_key'),$f3->get('s3.secret_key'),$f3->get('s3.endpoint'));
				if ($url = $sample->get('url')) {
					if ($s3) {
						$urlp = parse_url($url);
						$response = $s3->deleteObject( $f3->get('s3.bucket_samples'), basename($urlp["path"]) );
					}
					$sample->erase();
				}
			} else {
				//User is not owner -- Dont delete
			}
		} 
		if ($_REQUEST['load']) {
			$sample = new \Prismav\Sample($_REQUEST['load']);
			header('Content-type: application/json');
			echo json_encode($sample->fieldDump());
			die();
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") { //New Sample
			$job = array();
			$job['sample_id'] = $_POST['sample_id'];
			$job['url'] = $_POST['uploadURL'];
			$job['channel'] = $params['channel'];
			$job['author'] = User::userID($f3);
			$job['title'] = $_POST['name'];
			$job['tags'] = $_POST['tags'];
			$job['description'] = $_POST['description'];
			$job['type'] = $_POST['type'];

			if (isset($_FILES) and ($_FILES['upload']['size']) > 0) { //They are uploading a file
				//TODO Do a mimetype check for all available sound formats
				if ( \Prismav\SampleJob::validMimeType($_FILES['upload']['type']) ) {
					//File has been uploaded, move it to s3 w/ ttl set for temporary files
					$s3 = new S3($f3->get('s3.access_key'),$f3->get('s3.secret_key'),$f3->get('s3.endpointTemp'));
					if ($s3) {
						$path_parts = pathinfo($_FILES['upload']['name']);
						$file_contents = file_get_contents($_FILES['upload']['tmp_name']);
						$file_hash = crc32($file_contents);
						$file_name = $job['channel']."-".$file_hash.".".$path_parts['extension'];
						$response = $s3->putObject( $f3->get('s3.bucket_temp'), $file_name, $file_contents, array('Content-Type'=>$_FILES['upload']['type']) );
						if ($response->code == 200) { //Success
							$job['url'] = "https://s3.amazonaws.com/".$f3->get('s3.bucket_temp')."/".$file_name;
						} else { //TODO Notify of failed upload
							$job['error'] = 'Unable to connect to file storage : '.$response->error['message'];
							var_dump($response);
						}
					} else { //TODO Notify of failed s3 access;
						$job['error'] = 'Unable to connect to file storage';
					};
				} else {
					$job['error'] = 'Invalid file type';
				}
			} elseif ($_POST['uploadURL'] and !stristr($_POST['uploadURL'],$f3->get('s3.bucket_samples'))) {
				if (\Prismav\SampleJob::validUrl($job['url'])) {
					//Media is ok
				} else {
					$job['error'] = 'Invalid media url';
				}
			} elseif (!$_POST['uploadURL']) { //No source provided
				$job['error']='No Source provided';
			}
	
			if (isset($job['error'])) {
				$f3->set('error',$job['error']);
			} else {
				$que = \Que::instance();
				$job_id = $que->add('samples',$job);
				$notify = $f3->get('NOTIFY');
				$success = 'You have initiated a sample creation request '.substr($job_id,0,10).' in channel '.$job['channel'];
				$notify->addNotification('Create Sample',$success);
				User::checkAuth($f3);
				$f3->set('job_id',$job_id);
				$f3->set('success',$success);
			}
		}


		//Load Samples
		$f3->set('samples', \Prismav\Sample::fromChannel($params['channel'], User::userID($f3) ));

		$f3->set('content','admin/samples.htm');
		$f3->set('FULL',true);
		echo \Template::instance()->render('layout.htm');
	}

	//Used for sending large files
	function readfile_chunked($filename, $retbytes = TRUE) {
		$buffer = '';
		$cnt =0;
		$handle = fopen($filename, 'rb');
		if ($handle === false) {
			return false;
		}
		while (!feof($handle)) {
			$cnt++;
			$buffer = fread($handle, (1024 * 1024));
			echo $buffer;
			ob_flush();
			flush();
			if ($retbytes) {
				$cnt += strlen($buffer);
			}
		}
		$status = fclose($handle);
		if ($retbytes && $status) {
			return $cnt; // return num. bytes delivered like readfile() does.
		}
		return $status;
	}

	function ambiencePlay($f3,$params) {
		//Play ambient sound
		$sounds = $f3->get('ambience');
		if ($sound =  $sounds[$params['id']]) {
			$path = ROOT."/../sounds/".$sound['source'];
			if (!file_exists($path)) {
				die('Sound file missing.');
			} else {
				header("Content-Type: audio/wav");
				header("Content-Length: ".filesize($path));
				$this->readfile_chunked($path);
				die();
			}
		} else {
			die('Sound not found.');
		}
		
	}
}
?>
