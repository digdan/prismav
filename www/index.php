<?php

// Kickstart the framework
$f3=require('lib/base.php');
define('ROOT',dirname(__DIR__));


if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config(ROOT.'/configs/config.ini');

// Load routes
$f3->config(ROOT.'/configs/routes.ini');

$f3->set('LOG',new LogBat($f3->get('log.general'), $f3->get('log.level'))); //Create new logger
$f3->get('LOG')->log('Request Initiated',LogBat::DEBUG);

session_start();

//Connect to DB
$f3->set('DB', new DB\SQL( $f3->get('db.dsn'),$f3->get('db.user'),$f3->get('db.password')));
$f3->set('CACHE',true);

//Global vars
$f3->set('YEAR',date('Y'));
$f3->set('MONTH',date('m'));
$f3->set('DAY',date('j'));

//Default HUE / Channel
$f3->set('hue', 180 );
$f3->set('desaturation','100%');
$f3->set('channel', '');
$f3->set('channelSlug','');

//Load template functions for data filtering
\Prismav\Filter::assign($f3);

//Add Que Triggers
\Que::setTrigger('samples',$f3->get('cron.sampleScript'));
\Que::setTrigger('songs',$f3->get('cron.sampleSongs'));

//Check auth status
User::checkAuth($f3);

//Default sidebar
$f3->set('sidebar','sidebars/default.htm');

//Launch
$f3->run();
