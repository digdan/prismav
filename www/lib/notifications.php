<?php
	/**
	* Nofify class
	* An F3 lib to help with facebook style notifications
	*/
class Notifications extends \DB\SQL\Mapper {

	var $notifications;
	var $lastView;
	var $user_id;

	public function __construct($user_id=NULL) {
		parent::__construct( \Base::instance()->get('DB'), 'notifications' );
		if (is_null($user_id)) { //Empty notification object
			$this->set('notifications',json_encode(array()));
			$this->set('lastView',time());
		} else { //Try to load previous
			if (!$this->load(array("user_id=?",$user_id))) { //Load failed, create new one
				$this->set('user_id',$user_id);
				$this->set('notifications',json_encode(array()));
				$this->set('lastView',time());
				$this->save();
			}
		}
	}

	public function markRead() {
		$this->set('lastView',time());
		$this->save();
	}

	public function countUnread() {
		$unread = 0;
		if ($list = json_decode($this->get('notifications'))) {
			foreach($list as $k=>$v) {
				if ($v->created > $this->get('lastView')) $unread++;	
			}
		} else return 0;
		return $unread;
	}

	public function addNotification($title,$html,$iconClass=NULL) {
		$notification['created'] = time();
		$notification['title'] = $title;
		if (!is_null($iconClass)) $notification['iconClass'] = $iconClass;
		$notification['body'] = $html;
		$list = $this->getList();
		$list[] = $notification;
		$this->setList($list);
	}

	public function setList($list) {
		$this->set('notifications',json_encode($list));
		$this->save();
	}

	public function getList() {
		return array_reverse(json_decode($this->get('notifications')));
	}	
}
?>
