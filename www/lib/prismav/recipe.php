<?php
	namespace Prismav;

	class Recipe {
		var $struct; //Root structure of the recipe

		public __construct($string=NULL) {

			//Create emptry structure pattern.
			$this->struct = stdClass();
			$this->struct->samples = array();

			if (!is_null($string)) {
				$this->import($string);
			}
		}

		public function export() {
			return json_encode($struct);
		}

		public function import($string) {
			return $this->struct = json_decode($string);
		}


		public function addSample($sample_id,$args) {
			$this->struct['samples'][$sample_id] = array('id'=>$sample_id,'a'=>$args,'e'=>array());
		}

		public function addEffect($sample_id,$effect_name,$args) {
			$this->struct['samples'][$sample_id]['e'][] = array('n'=>$effect_name,'a'=>$args);
		}

		public function removeSample($sample_id) {
			unset($this->struct['samples'][$sample_id]);
		}

		public function removeEffect($sample_id,$effect_name) {
			unset($this->struct['samples'][$sample_id]);
		}

		
	}
?>
