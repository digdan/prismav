<?php 
	namespace Prismav;
	class SampleJob extends Job{ //Source sample
		var $target; //Path to temp sample file
		var $bpm; //Detected BPM
		var $key; //Musical key, array form
		var $buffer; //Output buffer for debugging
		var $inspector; //Wav Inspector

		function perform($f3) {
			$log = $f3->get('LOG');
			$url = $this->data('url');
			//Create temp dir for processing
			$this->createTable($f3->get('WORKTABLE'));
			$log->log('Created worktable');

			//Download URL
			$log->log("Downloading from {$url}");
			$size = $this->download($url);
			if (($size === false) || ($size == 0)) {
				return $this->error('Unable to download sample media');
			}

			//Convert to WAV
			$log->log("Converting sample for {$this->worktable}");
			if (! $this->convert()) {
				return $this->error('Unable to convert sample to WAV');
			}

			$log->log('Initiating Wav Inspector');
			$this->inspector = new \Prismav\WavInspector($this->worktable,'target.wav');
			$this->inspector->perform($f3);
			$this->data = array_merge($this->data,$this->inspector->data); //Merge data information

			$log->log("Building Spectrogram {$this->worktable}");
			if (! $this->buildImage()) {
				return $this->error('Unable to build spectrograph');
			}
			
			$log->log("Moving to Content Host {$this->worktable}");
			if (! $this->moveToS3($f3)) {
				return $this->error('Unable to move to host');
			}

			if ($this->data['sample_id']) {
				$sample = \Prismav\Sample::buildSample($this->data,$this->data['sample_id']);
			} else {
				$sample = \Prismav\Sample::buildSample($this->data);
			}
			$sample->save();
			return $this->success(); // All successful
		}

		function download($url) {
			if (stristr($url,"youtube.com")) { //Youtube URL, download audio
				$parts = parse_url($url);
				parse_str($parts['query'],$query);
				$realfilename = $query['v'].".wav";
				$this->target = $this->worktable."/".$realfilename;
				$command = "youtube-dl {$url} --extract-audio --audio-format wav --audio-quality 0 --output {$this->target}";
				shell_exec($command);
				return file_exists($this->target);
			} else {
				$content = get_headers($url,1);
				$content = array_change_key_case($content, CASE_LOWER);
				if ($content['content-disposition']) {
					$tmp_name = explode('=', $content['content-disposition']);
					if ($tmp_name[1]) $realfilename = trim($tmp_name[1],'";\'');
				} else  {
					$stripped_url = preg_replace('/\\?.*/', '', $url);
					$realfilename = basename($stripped_url);
				} 
				$this->target = $this->worktable."/".$realfilename;
				return file_put_contents($this->target, fopen($url, 'r'));
			}
			
		}

		function convert() { //Converts input media to wav
			$command = "ffmpeg -i {$this->target} ".$this->worktable."/target.wav";
			$this->buffer .= "Convert Command: ".$command."\n";
			exec($command,$buffer);
			$this->buffer .= join("\n",$buffer);
			return (file_exists($this->worktable."/target.wav"));
		}

		function buildImage() {
			$command = "sox {$this->worktable}/target.wav -n spectrogram -x 140 -Y 140 -r -l -h -o {$this->worktable}/target.png";
			$line = exec($command,$imageData);
			return true;
		}


		function moveToS3($f3) {
			$s3name = sha1_file( $this->worktable."/target.wav" );

			$wavFilename = $this->data['channel'].'-'.$s3name.".wav";
			$this->data['url'] = $f3->get('s3.prefix').'/'.$f3->get('s3.bucket_samples').'/'.$wavFilename;
			$command = "AWS_ACCESS_KEY_ID=".$f3->get('s3.access_key')." AWS_SECRET_ACCESS_KEY=".$f3->get('s3.secret_key')." aws s3 cp {$this->worktable}/target.wav s3://{$f3->get('s3.bucket_samples')}/{$wavFilename}";
			$line = exec($command,$wavData);

			$pngFilename = $this->data['channel'].'-'.$s3name.".png";
			$this->data['png'] = $f3->get('s3.prefix').'/'.$f3->get('s3.bucket_samples').'/'.$pngFilename;
			$command = "AWS_ACCESS_KEY_ID=".$f3->get('s3.access_key')." AWS_SECRET_ACCESS_KEY=".$f3->get('s3.secret_key')." aws s3 cp {$this->worktable}/target.png s3://{$f3->get('s3.bucket_samples')}/{$pngFilename}";
			$line = exec($command,$pngData);
	
			return true;
		}


		static function validMimeType($mimeType) {
			switch (strtolower($mimeType)) {
				case 'application/octet-stream':
				case 'audio/wav': 
				case 'audio/mpeg':
				case 'audio/mp3':
				case 'audio/x-aiff':
				case 'audio/ogg': 
					return true;
					break;
				default: return false; break;
			}
		}

                public function timeConvert($from) {
                        // 00:00:00.89
                        $parts = explode(":",$from);
                        $hour = $parts[0] * 60 * 60;
                        $minute = $parts[1] * 60;
                        $secondp = explode(".",$parts[2]);
                        $second = $secondp[0];

                        $totalMiliseconds = (($hour + $minute + $second) * 1000) + $secondp[1];

                        return $totalMiliseconds;
                }


		static function validUrl($url) {

			//Process exception urls first
			$parts = parse_url($url);
			if (strstr($parts['host'],'youtube.com')) {
				return true;
			}

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_NOBODY, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$results = explode("\n", trim(curl_exec($ch)));
			foreach($results as $line) {
				if (strtok($line, ':') == 'Content-Type') {
					$parts = explode(":", $line);
					return self::validMimeType( trim($parts[1]) );
				}
			}
			return false;
		}
	}
?>
