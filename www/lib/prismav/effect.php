<?php
	/** Class abstraction for all effects plugins **/
	namespace Prismav;
	class Effect {
		var $name='Default'; //Name of the effect
		var $target; //Location of the input sound file
		var $outFile; //The output of the effect is stored here
		var $parameters=array(); //Parmeters passed to this instance of the effect
		var $attributes; //Attributes of sample after the effect has been applied.

		static $parameters=array( //Static map of allowable parameters and type of data accepted
			'source'=>'sample_id'
		);

		function __construct( $target = NULL ) {
			if (!is_null($target)) $this->target = $target;
		}

		function run($target=NULL) { //Main entrypoint of effects.
			if (!is_null($target)) $this->target = $target;
			return $target;
		}

		static function filename($ext="wav") {
			return md5( microtime( TRUE ) ).".".$ext;
		}

		static function parameters() { //Show list of parameters to help build interfaces
			return self::$parameters;
		}
	}
?>
