<?php
	//Filters are functions used in the F3 Template that can be used to link to data. Given a user_id it should link to a users name and profile
	namespace Prismav;
	class Filter {
		static function assign($f3) {
			$functions = array(
				'userId'=>function($user_id) {
					$f3 = \Base::instance();
					$admin = new \DB\SQL\Mapper($f3->get('DB'), 'admins');
					$admin->load(array('id=?',$user_id));
					return $admin->get('display_name');
				},
			);
			foreach($functions as $name=>$function) {
				$f3->set('FILTER.'.$name,$function);
			}
		}
	}
?>
