<?php
	namespace Prismav;
	class Song extends \DB\SQL\Mapper {
		public function __construct($song_id=NULL) {
			parent::__construct( \Base::instance()->get('DB'), 'songs' );
			if (is_null($song_id)) { //Empty object
				$this->setInfo(array());
				$this->set('created',time());
				$this->setRecipe( new \stdClass() );
			} else { //Try to load previous
				if (!$this->load(array("id=?",$song_id))) { //Load failed, create new one
					return false;
				}
			}
		}
		
		public function setInfo($info) {
			if (count($info)) {
				$this->set('title',$info['title']);
				$this->set('description',$info['description']);
			} else {
				$this->set('title','Unnamed');
				$this->set('description','Undescribed');
			}
		}

		public function setRecipe($recipeObj) {
			$this->set('recipe',json_encode($recipeObj));
		}
	
		public function getRecipe() {
			return json_decode( $this->get('recipe') );
		}

		public function setAuthor($user_id) {
			$this->set('user_id',$user_id);
		}

		public function setChannel($channel) {
			$this->set('channel',$channel);
		}

		public function timeConvert($from) {
			// 00:00:00.89
			$parts = explode(":",$from);
			$hour = $parts[0] * 60 * 60;
			$minute = $parts[1] * 60;
			$secondp = explode(".",$parts[2]);
			$second = $secondp[0];

			$totalMiliseconds = (($hour + $minute + $second) * 1000) + $secondp[1];

			return $totalMiliseconds;
		}

		public function fieldDump() {
			$out = array();
			foreach($this->fields as $name=>$field) {
				$out[$name] = $field['value'];
			}
			return $out;
		}

		static function buildSong( $info, $song_id=NULL ) {
			$song = new static($song_id);
			$song->set('url',$info['url']);
			$song->set('recipe',$info['recipe']);
			$song->set('channel',$info['channel']);
			$song->set('user_id',$info['author']);
			$song->set('title',$info['title']);
			$song->set('description',$info['description']);
			$song->set('length',$song->timeConvert($info['duration']));
			$song->set('size',$info['file-size']);
			$song->set('musicBPM',$info['bpm']);
			if (is_array($info['key'])) {
				array_pop($info['key']); //Remove certainty raiting
				$song->set('musicKey', join(" ",$info['key']));
			} else {
				$song->set('musicKey',$info['key']);
			}
			$song->set('keys',$info['keys']);
			return $song;
		}

		static function fromChannel( $channel,$userId=NULL ) {
			$song = new static();
			if (is_null($userId)) {
				$records = $song->find(array('channel=?',$channel));
				return $records;
			} else { //Do not include other users base songs
				$records = $song->find(array("channel=? AND user_id=?",$channel,$userId));
				return $records;
			}
		}
		
	}
?>
