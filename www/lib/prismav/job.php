<?php 
	namespace Prismav;
	class Job {
		const TTL = (3 * 60 * 60 * 24); //3 Days -- TTL for job directory to exist
		var $data;
		var $message; //Used for notifications of success
		var $lastError;
		var $worktable; //Path to working directory

		function __construct($data=NULL) {
			if (!is_null($data)) $this->data = $data;
		}

		public function data($key,$value=NULL) { //Public getter setter for job data
			if (is_null($value)) {
				return $this->data[$key];
			} else {
				return $this->data[$key] = $value;
			}
		}

		public function error($str) {
			echo "Error : {$str}\n";
			$this->lastError = $str;
			return false;
		}

		public function success($str=null) {
			$this->message = $str;
			return true;
		}

		public function getError() {
			return $this->lastError;
		}

		public function createTable($basePath) {
			$dirs = glob($basePath."/*");
			foreach($dirs as $dir) { //Do some garbage collecting
				if (is_dir($dir)) {
					if (time()-filemtime($filename) > self::TTL) { //Directory is older than job ttl
						system('rm -rf '.$dir);
					}
				}
			}
			$newDir = $basePath . '/' . md5(uniqid('',true));
			mkdir($newDir);
			$this->worktable = $newDir;
			return $newDir;
		}

		function perform($f3) {
			echo "Performing job on ";
			var_dump($this->data);
			$this->lastError = 'Unable to run';
			return $true;
		}

		function cleanup() {}
	}
?>
