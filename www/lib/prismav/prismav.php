<?php 
	class PrismavJob {
	}

	class PrismavSampleJob extends PrismavJob{ //Source sample
	}

	class PrismavSongJob extends PrismavJob {
		
	}

	class Prismav {
		var $workingDir;

		function __construct($workingDir) {
			if (is_dir($workingDir)) {
				$this->workingDir = $workingDir;
			} else {
				return false;
			}
		}
			
		static function urlMimeType($url) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_NOBODY, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$results = explode("\n", trim(curl_exec($ch)));
			foreach($results as $line) {
				if (strtok($line, ':') == 'Content-Type') {
					$parts = explode(":", $line);
					return trim($parts[1]);
				}
			}
			return false;
		}

		function processSample(prismavSampleJob $job) {
		}

	}
?>
