<?php
	namespace Prismav;
	class Sample extends \DB\SQL\Mapper {
		public function __construct($sample_id=NULL) {
			parent::__construct( \Base::instance()->get('DB'), 'samples' );
			if (is_null($sample_id)) { //Empty object
				$this->setInfo(array());
				$this->set('created',time());
			} else { //Try to load previous
				if (!$this->load(array("id=?",$sample_id))) { //Load failed, create new one
					return false;
				}
			}
		}
		
		public function setInfo($info) {
			$this->set('title',$info['title']);
			$this->set('description',$info['description']);
			$this->set('tags',$info['tags']);
	//		$this->set('info',json_encode($info));
		}

		public function setAuthor($user_id) {
			$this->set('user_id',$user_id);
		}

		public function setChannel($channel) {
			$this->set('channel',$channel);
		}

		public function timeConvert($from) {
			// 00:00:00.89
			$parts = explode(":",$from);
			$hour = $parts[0] * 60 * 60;
			$minute = $parts[1] * 60;
			$secondp = explode(".",$parts[2]);
			$second = $secondp[0];

			$totalMiliseconds = (($hour + $minute + $second) * 1000) + $secondp[1];

			return $totalMiliseconds;
		}

		public function fieldDump() {
			$out = array();
			foreach($this->fields as $name=>$field) {
				$out[$name] = $field['value'];
			}
			return $out;
		}

		static function buildSample( $info, $sample_id=NULL ) {
			$sample = new static($sample_id);
			$sample->set('url',$info['url']);
			$sample->set('png',$info['png']);
			$sample->set('type',$info['type']);
			$sample->set('channel',$info['channel']);
			$sample->set('user_id',$info['author']);
			$sample->set('title',$info['title']);
			$sample->set('description',$info['description']);
			$sample->set('tags',$info['tags']);
			$sample->set('length',$sample->timeConvert($info['duration']));
			$sample->set('size',$info['file-size']);
			$sample->set('musicBPM',$info['bpm']);
			if (is_array($info['key'])) {
				array_pop($info['key']); //Remove certainty raiting
				$sample->set('musicKey', join(" ",$info['key']));
			} else {
				$sample->set('musicKey',$info['key']);
			}
			$sample->set('keys',$info['keys']);
			return $sample;
		}

		public function effectExport() { //Export to use with effects
			return array(
				'id'=>$this->get('id'),
				'url'=>$this->get('url'),
				'length'=>$this->get('length'),
				'size'=>$this->get('size'),
				'bpm'=>$this->get('musicBPM'),
				'key'=>$this->get('musicKey'),
				'path'=>''
			);
		}

		static function fromChannel( $channel,$userId=NULL, $base=false ) {
			$sample = new static();
			if (is_null($userId)) {
				$records = $sample->find(array('channel=?',$channel));
				return $records;
			} else { //Do not include other users base samples
				if ($base) {
					$records = $sample->find(array("(channel=? and type='base' and user_id=?)",$channel,$userId));
				} else {
					$records = $sample->find(array("channel=? and (type <> 'base' or (type='base' and user_id=?))",$channel,$userId));
				}
				return $records;
			}
		}

		
	}
?>
