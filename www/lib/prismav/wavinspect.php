<?php 
	namespace Prismav;
	class WavInspect { 
		var $target; //Path to temp sample file
		var $bpm; //Detected BPM
		var $key; //Musical key, array form
		var $buffer; //Output buffer for debugging
		var $data; //Holds inspection information
		var $worktable;

		function __construct($worktable,$target) {
			$this->worktable = $worktable;
			$this->target = $worktable."/".$target;
			$this->bpm = 0;
			$this->key = "Unknown";			
		}

		function perform($f3) {
			$log = $f3->get('LOG');

			//Build sound graphics
			$log->log("Gathering wav information {$this->target}");
			if (! $this->getInfo()) {
				return $this->error('Unable to analyze Wav file');
			}


			$log->log("Analyzing BPMs for {$this->target}");
			if (! $this->analyzeBPM()) {
				return $this->error('Unable to analyze WAV file');
			}

			//Analyze note tonality / key
			$log->log("Analyzing Notes for {$this->target}");
			if (! $this->analyzeNotes($f3)) {
				return $this->error('Unable to analyze Notes');
			}
			
			return $this->success(); // All successful
		}

		function analyzeBPM() {
			switch($this->data['type']) {
				case 'sound':
				case 'tonalSound':
					return true;
				case 'loop':
				case 'tonalLoop':
					$this->data['bpm'] = $this->bpmer($this->timeConvert($this->data['duration']));
					return true;
					break;
			}

			//Detect BPM
			$command = "soundstretch {$this->target} -bpm 2>&1";
        		$line = exec($command,$bpmData);
        		$bpm = 0;
        		foreach($bpmData as $bpmLine) {
                		if (strstr($bpmLine,"Detected BPM rate")) {
                        		$bpm = substr($bpmLine,18);
					$this->data['bpm'] = $bpm;
					return true;
                		}
				if (strstr($bpmLine,"Couldn't detect BPM rate.")) {
					$this->data['bpm'] = 0;
					return true;
				}
				
        		}
			return false;
		}

		function analyzeNotes($f3) {
			switch($this->data['type']) {
				case 'sound':
				case 'loop':
					return true;
			}

			$command = dirname($f3->get('WORKTABLE'))."/trimWav.sh {$this->target} {$this->worktable}/target-trimmed.wav";
			$line = exec($command,$trimData);

			$command = dirname($f3->get('WORKTABLE'))."/waon -i {$this->worktable}/target-trimmed.wav -o {$this->worktable}/target.mid -n 4096 -s 2048 -w 6 2>&1";
			$line = exec($command,$noteData);

			$seconds = floor($this->timeConvert($this->data['duration']) / 1000); //Convert sample lenght into seconds
			if ($seconds > 5) {
				//Trim middle third for example
				$thirdpoint = floor($seconds / 3);
				$bounds = "{$thirdpoint} {$thirdpoint}";
			} else {
				$bounds = "0,5";
			}
	
			//Analyze notes and scale
			$command = dirname($f3->get('WORKTABLE'))."/getKey.py {$this->worktable}/target.mid 2>&1";
			exec($command,$keyData);
			$line = $keyData[1];	
			$line = str_replace("(","[",$line);
			$line = str_replace(")","]",$line);
			$line = str_replace("'","\"",$line);
			$this->data['key'] = json_decode($line);
			array_shift($keyData);
			$this->data['keys'] = $keyData;
			return true;
		}

		function getInfo() {
			$command = "sox --info {$this->target} 2>&1";
			$line = exec($command,$wavData);
			$map = array();
			foreach($wavData as $wavLine) {
				if (strstr($wavLine,":")) {
					$wavLineParts = explode(": ",trim($wavLine));
					if (strstr($wavLineParts[1]," = ")) { //Reformat duration
						$sub = explode(" = ",$wavLineParts[1]);
						$wavLineParts[1] = $sub[0];
					}
					$key = strtolower(str_replace(" ","-",trim($wavLineParts[0])));
					$value = trim($wavLineParts[1]);
					$this->data[$key] = $value;
				}
			}
			$this->data['file-size'] = filesize($this->target);
			return true;
		}

		static function validMimeType($mimeType) {
			switch (strtolower($mimeType)) {
				case 'application/octet-stream':
				case 'audio/wav': 
				case 'audio/mpeg':
				case 'audio/mp3':
				case 'audio/x-aiff':
				case 'audio/ogg': 
					return true;
					break;
				default: return false; break;
			}
		}

                public function timeConvert($from) {
                        // 00:00:00.89
                        $parts = explode(":",$from);
                        $hour = $parts[0] * 60 * 60;
                        $minute = $parts[1] * 60;
                        $secondp = explode(".",$parts[2]);
                        $second = $secondp[0];

                        $totalMiliseconds = (($hour + $minute + $second) * 1000) + $secondp[1];

                        return $totalMiliseconds;
                }


		public function bpmer($miliseconds) {
			//Give a BPM between 100 and 200 that is a multiple of the provided miliseconds
			$base = (60000 / $miliseconds);
			while($base < 100) $base = $base * 2;
			return round($base,2);
		}
	}
?>
