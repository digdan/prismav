<?php
class LogBat extends Log {
	var $logLevel;
	const PRODUCTION=1;
	const STAGING=2;
	const DEVELOPMENT=3;
	const DEBUG=4;

	function __construct($file,$level=4) {
		parent::__construct($file);
		$this->logLevel = $level;
	}

	function log($text,$level=3) {
		if ($level <= $this->logLevel) {
			$this->write($text);
		}
	}
}
