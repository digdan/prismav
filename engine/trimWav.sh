wl=`sox $1 -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p'`
dur=`echo $wl | perl -nl -MPOSIX -e 'print ceil($_ / 3);'`
sox $1 $2 trim $dur 5
