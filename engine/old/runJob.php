#!/usr/bin/php -q
<?php

	/*****
		Builds a Primav based on job parameters.

		Job parameters : 
			ytURL = youtube url to use as source
			stretch = the numeric value of what to stretch wav by ( default 8 )
			actions = array of actionable items determined by 'type' : 

				type = stretch
					stretch = amount to stretch wav by

				type = tremolo
					multiplier = amount to multiply hertz to get frequency
					frequency = if multiplier not supplied, then give an explicit frequency
					depth = amount of depth in tremolo, from 0 to 100
	*/


	//Include all functions
	$procList = glob(__DIR__."/procs/*.php");
	foreach($procList as $procItem) {
		include($procItem);
	}

	//Look up job information from database based on job id
	
	//Test if job is running

	//if not then start a new job

	//Create working directory
	$dir = __DIR__."/temp/".uniqid();
	mkdir($dir);

	if (strstr($job['source'],'youtube') { //Youtube URL provided
		$command = "/usr/bin/youtube-dl --audio-format wav --audio-quality 0 --extract-audio ".$job['ytURL']." --user-agent ViscousBot -o {$dir}/original.%\(ext\)s";
		echo "Downloading from Youtube.\n";
		exec($command);
	} else {
		if (file_exists($job['source'])) {
			echo "Moving original\n";
			$command = "mv {$job['source']} {$dir}/original.wav";
			exec($command);
		}
	}

	$target = $dir."/original.wav";

	if (!file_exists($target)) {
		die('No source wav to work with');
	}

	echo "Detecting tempo\n";
	$tempoData = pr_detect_bpm($target);
	$bpm = $tempoData['bpm'];
	$hertz = $tempoData['hertz'];

	foreach($job['actions'] as $action) {
		switch ($action['type']) {
			case 'stretch' : 
				pr_stretch($target,$action['stretch']);
				break;
			case 'tremolo' : 
				if ($action['multiplier']) $freq = ($action['multiplier'] * $hertz);
				if ($action['frequency']) $freq = $action['frequency'];
				pr_tremolo($target,$frequency,$action['depth']);
				break;
			case 'trim' :
				pr_trim_silence($target);
				break;
		}
	}

	

?>
