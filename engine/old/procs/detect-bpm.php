<?php
	function pr_detect_bpm($target) {
		$command = "soundstretch {$target} -bpm 2>&1";
		echo "Calculating BPM\n";
		$line = exec($command,$bpmData);
		$bpm = 0;
		foreach($bpmData as $bpmLine) {
			if (strstr($bpmLine,"Detected BPM rate")) {
				$bpm = substr($bpmLine,18);
			}
		}

		$hertz = round(($bpm / 60),4);
		return array('bpm'=>$bpm,'hertz'=>$hertz);
	}
?>
